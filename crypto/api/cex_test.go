package api_test

import (
	"testing"

	"sarahg.dev/go/crypto/api"
)

// Test that code returns an error if an invalid argument is passed to it
func TestAPICall(t *testing.T) {
	_, err := api.GetRate("")
	if err == nil {
		t.Error("Error not found")
	}
}
